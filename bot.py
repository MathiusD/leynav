import os
import discord

from leynavfunc.funct_json import init_json_content, init_json_patern, search_json, read_json_to_discord
from leynavfunc.msg_traitement import reponse_send, embed_send, embed_create
from leynavfunc.random import roll
from dotenv import load_dotenv
from discord.ext import commands

load_dotenv()
token = os.getenv('DISCORD_TOKEN')
prefix = os.getenv('DISCORD_PREFIX')
content = os.getenv('DISCORD_CONTENT')
path_patern = os.getenv('DISCORD_PATERN')
game = os.getenv('DISCORD_GAME')
max_dice = os.getenv('DISCORD_MAX_DICE')
max_dice_face = os.getenv('DISCORD_MAX_DICE_FACE')

patern = init_json_patern(path_patern)
files = init_json_content(content)

bot = commands.Bot(command_prefix=prefix)

#Fonction lancée au démarrage du bot
@bot.event
async def on_ready():
    act = discord.Game(game)
    await bot.change_presence(status=discord.Status.online, activity=act, afk=False)
    print(
        f'{bot.user.name} is connected and played at {bot.activity} !'
    )

#Fonction d'affectation des commandes à la lecture des Fichiers json
for i in range(len(patern)):
    #La Fonction en question
    @bot.command(name = patern[i]['name'], help = patern[i]['help'])
    async def read_json_to_discord_command(context, arg1 = '', arg2 = ''):
        out = search_json(files, patern, context.command.name, context.message.guild.id)
        if len(out) > 0:
            traitement = read_json_to_discord(out, files, patern, arg1, arg2)
            for i in range(len(traitement)):
                if len(traitement[i]['reponse']['message']) > 0:
                    embed = None
                if len(traitement[i]['reponse']['embed']) > 0:
                    embed = discord.Embed(title = traitement[i]['reponse']['embed']['title'], description = traitement[i]['reponse']['embed']['description'])
                    await reponse_send(context, traitement[i]['reponse']['message'], embed)
                if len(traitement[i]['embed']) > 0:
                    await embed_send(context, embed_create(traitement[i]['embed']['message'], traitement[i]['embed']['name']))
        else:
            await reponse_send(context, 'Error 404 : Data Not Found')

#Fonction de lancé de dés
@bot.command(name = 'roll', help = 'Commande afin de lancer des dés\n    ~roll x y avec x le nombre de lancé et y le nombre de face\nExemple :\n    ~roll 1 6 soit le lancé d\'un dé 6')
async def roll_command(context, arg1 = '', arg2 = ''):
    if arg1 <= max_dice and arg2 <= max_dice_face:
        await reponse_send(context, roll(arg1, arg2))
    else:
        await reponse_send(context, "NAHMEHO Je suis pas ton chien")

#Fonction de renvoi vers les documentation
@bot.command(name = 'docs_pdf', help = 'Commande afin de récupérer les pdf éditables\n    ~docs_pdf x y avec x la catégorie demandé et y le fichier précis\nExemple :\n    ~docs_pdf pj equipement soit la demande du fichier de la fiche d\'équipement pour les joueurs')
async def docs_pdf_comand(context, arg1 = '', arg2 = ''):
    await reponse_send(context, "Non implémentée")

bot.run(token)
