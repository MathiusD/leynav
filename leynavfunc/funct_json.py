import os
import json

def init_json_content(content):
    os.chdir(content)
    filesr=[]
    for direc in os.walk(content):
        for j in range(len(direc[1])):
            contents = []
            os.chdir(content + direc[1][j])
            for files in os.walk(content + direc[1][j]):
                for i in range(len(files[2])):
                    if '.json' in files[2][i]:
                        contents.append(files[2][i])
            for i in range(len(contents)):
                files_brut = open(contents[i], 'r')
                filesr.append(json.load(files_brut))
    return filesr

def init_json_patern(content):
    os.chdir(content)
    contents = []
    for files in os.walk(content):
        for i in range(len(files[2])):
            if '.json' in files[2][i]:
                contents.append(files[2][i])
    files=[]
    for i in range(len(contents)):
        files_brut = open(contents[i], 'r')
        files.append(json.load(files_brut))
    return files

def search_json(files, patern, name, guild_id=0):
    output = []
    for j in range(len(patern)):
        if name == patern[j]['name']:
            for i in range(len(files)):
                if name == files[i]['name'] and ((guild_id or 'all') in files[i]['allow_in']):
                    out = []
                    out.append(i)
                    out.append(j)
                    output.append(out)
            return output
    return output

def add_from_patern(patern_cle, files_cle, list_files, list_patern):
    output = ''
    if patern_cle['action'] == 'display':
        output = patern_cle['options']['content'][0] + files_cle + patern_cle['options']['content'][1]
    elif patern_cle['action'] == 'read':
        add = output_patern()
        for j in range(len(patern_cle['options']['content'])):
            for i in range(len(files_cle)):
                out = search_json(list_files, list_patern, patern_cle['options']['content'][j])
                if len(out) > 0:
                    add = add + read_json_to_discord(list_files, list_files[out[0]], list_patern, files_cle[i]['arg1'], files_cle[i]['arg2'])
                    for i in range(len(add)):
                        output = output + add[i]["embed"]["message"]
                else:
                    output = 'Fichier(s) non trouvés'
    else:
        output = files_cle
    return output

def output_patern():
    output = {
        "reponse" : {"message":"", "embed":{}},
        "embed" : {"message":"", "name":""}
    }
    return output

def arg_not_found(files_content):
    output = output_patern()
    output['reponse']['message'] = "Catégorie Inconnue."
    reponse = 'Les catégories disponibles sont:\n\n'
    for cle, value in files_content.items():
        reponse = reponse + cle + '\n'
    output['embed']['message'] = reponse
    return output

def read_json_to_discord_all(list_files, files, list_patern, patern, arg1):
    output = output_patern()
    if arg1 == '':
        reponse = ''
        for cle, value in files['content'].items():
            add = add_from_patern(patern['content'][cle], files['content'][cle], list_files, list_patern)
            reponse = reponse + add
        output['embed']['message'] = reponse
        output['embed']['name'] = files['name']
    else:
        if arg1 in files['content']:
            output['embed']['message'] = add_from_patern(patern['content'][arg1], files['content'][arg1], list_files, list_patern)
        else:
            output = arg_not_found(files['content'])
    return output

def read_json_to_discord_one(list_files, files, list_patern, patern, arg1, arg2):
    output = output_patern()
    if arg1 == '':
        output['embed']['message'] = patern['content']['default']['msg']
        if patern['content']['default']['act'] == 'list':
            reponse = 'Les catégories disponibles sont :\n\n'
            for cle, value in files['content'].items():
                reponse = reponse + cle + '\n'
            output['embed']['message'] = reponse
        elif patern['content']['default']['act'] == 'all':
            reponse = ''
            for cle, value in files['content'].items():
                if cle != "default":
                    reptemp = "**__" + cle + "__**\n\n"
                    for cletemp, valuetemp in files['content'][cle].items():
                        add = add_from_patern(patern['content']['patern'][cletemp], files['content'][cle][cletemp], list_files, list_patern)
                        reptemp = reptemp + add
                    reponse = reponse + reptemp
            output['embed']['message'] = reponse
            output['embed']['name'] = files['name']
    else:
        if arg1 in files['content']:
            if arg2 == '':
                reponse = ''
                for cle, value in files['content'][arg1].items():
                    add = add_from_patern(patern['content']['patern'][cle], files['content'][arg1][cle], list_files, list_patern)
                    reponse = reponse + add
                output['embed']['message'] = reponse
                output['embed']['name'] = arg1
            else :
                if arg2 in files['content'][arg1]:
                    output['embed']['message'] = add_from_patern(patern['content']['patern'][arg2], files['content'][arg1][arg2], list_files, list_patern)
                else:
                    output = arg_not_found(files['content'][arg1])
        else:
            output = arg_not_found(files['content'])
    return output

def read_json_to_discord(out, list_files, list_patern, arg1='', arg2=''):
    output = []
    for i in range(len(out)):
        if list_patern[out[i][1]]['comportement'] == "all":
            output.append(read_json_to_discord_all(list_files, list_files[out[i][0]], list_patern, list_patern[out[i][1]], arg1))
        elif list_patern[out[i][1]]['comportement'] == "one":
            output.append(read_json_to_discord_one(list_files, list_files[out[i][0]], list_patern, list_patern[out[i][1]], arg1, arg2))
        else:
            out = output_patern()
            out['reponse']['message'] = "Comportement non défini"
            output.append(out)
    return output
