import discord

from discord.ext import commands

#Fonction de découpe, bête et méchante qui découpe le texte passé en argument en tableau contenant
#au maximum le maximum passé en argument
def text_format(txt, maxi = 2000):
    tab_txt = []
    if len(txt) > maxi:
        for i in range(int(len(txt) / maxi)):
            txt_coup = ''
            for j in range(maxi):
                txt_coup = txt_coup + txt[(maxi * i) + j]
            tab_txt.append(txt_coup)
        txt_coup = ''
        i = int(len(txt) / maxi) * maxi
        while i < len(txt):
            txt_coup = txt_coup + txt[i]
            i = i + 1
        tab_txt.append(txt_coup)
    else:
        tab_txt.append(txt)
    return tab_txt

#Fonction de découpe qui découpe le texte passé en argument en tableau contenant
#au maximum le maximum passé en argument et en respectant le découpage des mots
def text_format_with_respect_words(txt, maxi = 2000):
    tab_txt = []
    if len(txt) > maxi:
        splits = txt.split(' ')
        txt_coup = ''
        compt = 0
        while compt < len(txt):
            for i in range(len(splits)):
                if len(txt_coup + splits[i]) > maxi:
                    tab_txt.append(txt_coup)
                    txt_coup = ''
                while txt[compt] == ' ':
                    txt_coup =  txt_coup + txt[compt]
                    compt = compt + 1
                for j in range(len(splits[i])):
                    txt_coup =  txt_coup + splits[i][j]
                    compt = compt + 1
        if len(txt_coup) > 0:
            tab_txt.append(txt_coup)
    else:
        tab_txt.append(txt)
    return tab_txt

#Fonction d'envoi de message avec le splitage si nécessaire ainsi que l'ajout d'un embed commun à
#tout les messages si il est renseigné
async def reponse_send(ctx, msg, embd = None, fil=None, typing=True):
    if msg != "":
        tab = text_format_with_respect_words(msg)
        for i in range(len(tab)):
            await ctx.send(tab[i], embed = embd, file= fil)
            if typing == True and i < len(tab) - 1:
                await ctx.trigger_typing()

#Fonction de création d'Embed, on lui passe le texte, l'éventuel titre et elle retourne un tableau
#d'Embed
def embed_create(txt, name = None):
    tab_embed = []
    tab_txt = text_format_with_respect_words(txt, 2048)
    for i in range(len(tab_txt)):
        embed = discord.Embed(title = name, description = tab_txt[i])
        tab_embed.append(embed)
    return tab_embed

#Fonction d'envoi d'Embed
async def embed_send(ctx, embd, typing=True):
    for i in range(len(embd)):
        await ctx.send('', embed=embd[i])
        if typing == True and i < len(embd) - 1:
            await ctx.trigger_typing()
