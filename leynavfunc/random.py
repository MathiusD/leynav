from random import uniform

def roll(nb_des, nb_face):
    if nb_des == '' and nb_face == '':
        return 'Argument manquants'
    else:
        output = ""
        for i in range(int(nb_des)):
            temp = str(int(round(uniform(1, int(nb_face)), 0)))
            output = output + ' ' + temp
        return output